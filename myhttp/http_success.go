package main

import (
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/ma6174/slowrw"
)

func handle(rw http.ResponseWriter, req *http.Request) {
	go func() {
		buf := make([]byte, 1)
		n, err := io.ReadFull(req.Body, buf)
		if err != nil || n != 1 {
			log.Fatal("read failed:", n, err)
		}
		if buf[0] != '1' {
			log.Fatalf("buf not start at 1: %#v", string(buf))
		}
		out, err := os.Open(os.DevNull)
		if err != nil {
			log.Fatal(err)
		}
		io.Copy(out, req.Body)
	}()
	time.Sleep(time.Second)
}

type Reader struct {
	Reader io.ReaderAt
	Offset int64
}

func (p *Reader) Read(val []byte) (n int, err error) {
	n, err = p.Reader.ReadAt(val, p.Offset)
	p.Offset += int64(n)
	return
}

func (p *Reader) Close() error {
	if rc, ok := p.Reader.(io.ReadCloser); ok {
		return rc.Close()
	}
	return nil
}

func main() {
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	go func() {
		http.HandleFunc("/", handle)
		log.Fatal(http.ListenAndServe(":9999", nil))
	}()

	time.Sleep(time.Second * 2)

	reader := strings.NewReader("1234567890abcdefghigklmnopqrst")
	sr := slowrw.NewReaderAt(reader, time.Millisecond)
	ra := &Reader{reader, 0}
	req, err := http.NewRequest("POST", "http://127.0.0.1:9999/", ra)
	if err != nil {
		log.Fatal(err)
	}
	client := http.Client{
		// Timeout: time.Millisecond * 10,
		Timeout: time.Second * 3,
	}
	for {
		_, err := client.Do(req)
		if err != nil {
			log.Println(err)
		}
		sr = slowrw.NewReaderAt(reader, time.Millisecond)
		reader2 := &Reader{sr, 0}
		req.Body = reader2
	}
}
