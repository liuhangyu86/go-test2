package main

import (
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/ma6174/slowrw"
)

func handle(rw http.ResponseWriter, req *http.Request) {
	go func() {
		buf := make([]byte, 1)
		n, err := io.ReadFull(req.Body, buf)
		if err != nil || n != 1 {
			log.Fatal("read failed:", n, err)
		}
		if buf[0] != '1' {
			log.Fatalf("buf not start at 1: %#v", string(buf))
		}
		out, err := os.Open(os.DevNull)
		if err != nil {
			log.Fatal(err)
		}
		io.Copy(out, req.Body)
	}()
	time.Sleep(time.Second)
}

func main() {
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	go func() {
		http.HandleFunc("/", handle)
		log.Fatal(http.ListenAndServe("0.0.0.0:9999", nil))
	}()

	time.Sleep(time.Second * 2)

	reader := strings.NewReader("1234567890abcdefghigklmnopqrst")
	sr := slowrw.NewReadSeeker(reader, time.Millisecond)
	req, err := http.NewRequest("POST", "http://127.0.0.1:9999/", sr)
	if err != nil {
		log.Fatal(err)
	}
	client := http.Client{
		Timeout: time.Millisecond * 10000,
		Transport: &http.Transport{
			ResponseHeaderTimeout: time.Millisecond * 1000,
		},
	}
	for {
		_, err := client.Do(req)
		if err != nil {
			log.Println(err)
		}
		_, err = sr.Seek(0, 0)
		if err != nil {
			log.Fatal("seek failed", err)
		}
	}
}
